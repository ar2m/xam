import {RETRIEVE_TOKEN_ACTION, SIGNIN_ACTION, SIGNOUT_ACTION, SIGNUP_ACTION} from "../actions"

export const loginReducer = (prevState, action) => {
    switch (action.type) {
        case RETRIEVE_TOKEN_ACTION:
            return {
                ...prevState,
                userToken: action.userToken,
                isLoading: false
            }
        case SIGNIN_ACTION:
            return {
                ...prevState,
                userName: action.userName,
                userToken: action.userToken,
                isLoading: false
            } 
        case SIGNOUT_ACTION:
            return {
                ...prevState,
                userName: null,
                userToken: null,
                isLoading: false
            } 
        case SIGNUP_ACTION:
            return {
                ...prevState,
                userName: action.userName,
                userToken: action.userToken,
                isLoading: false
            }
    }
}
