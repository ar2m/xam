import { news_url, country_code, category, from, to, sortBy, apiKey } from "../config/rest-config";

export const fetchNews = async () => {
    try {
        const url = `${news_url}?country=${country_code}&category=${category}&from=${from}&to=${to}&sortBy=${sortBy}`;
        
        let news = await fetch(url, {
            headers: {
                'X-API-KEY': apiKey
            }
        })

        let result = await news.json();
        return result.articles;

    } catch (error) {
        throw error;
    }
}