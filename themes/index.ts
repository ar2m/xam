import {
    DefaultTheme as NavigationDefaultTheme,
    DarkTheme as NavigationDarkTheme,
  } from "@react-navigation/native";
  import {
    DefaultTheme as PaperDefaultTheme,
    DarkTheme as PaperDarkTheme,
  } from "react-native-paper";

  const CustomDefaultTheme = {
    ...NavigationDefaultTheme,
    ...PaperDefaultTheme,
    colors: {
      ...NavigationDefaultTheme.colors,
      ...PaperDefaultTheme.colors,
      background: "#fff",
      text: "#333",
    },
  };

  const CustomDarkTheme = {
    ...NavigationDarkTheme,
    ...PaperDarkTheme,
    colors: {
      ...NavigationDarkTheme.colors,
      ...PaperDarkTheme.colors,
      background: "#fff",
      text: "#333",
    },
  };

export { CustomDefaultTheme, CustomDarkTheme }