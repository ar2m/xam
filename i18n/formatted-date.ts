import moment from 'moment';

import * as config from './i18n';

const date = {
    /**
     * Load library, setting its initial locale
     *
     * @param {string} locale
     * @return Promise
     */
    init: (locale: string) => {
        return new Promise((resolve, reject) => {
            // @ts-ignore
            config.supportedLocales[locale]
                .momentLocaleLoader()
                .then(() => {
                    moment.locale(locale);

                    return resolve();
                })
                .catch((err: object) => reject(err));
        });
    },

    /**
     * @param {Date} date
     * @param {string} format
     * @return {string}
     */
    format: (date: Date, format: string): string => {
        return moment(date).format(format);
    }
}

export default date;
