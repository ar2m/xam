import { ar, en, fr } from "./translations";

export const fallback = "en";

export const supportedLocales = {
    en: {
        name: "English",
        translationFileLoader: () => en,

        // en is default locale in Moment
        momentLocaleLoader: () => Promise.resolve(),
    },
    ar: {
        name: "عربي",
        translationFileLoader: () => ar,
        // @ts-ignore
        momentLocaleLoader: () => import('moment/locale/ar'),
    },
    fr: {
        name: "English",
        translationFileLoader: () => fr,

        // en is default locale in Moment
        momentLocaleLoader: () => Promise.resolve(),
    },
};

export const defaultNamespace = "common";

export const namespaces = [
    "common",
    "SplashScreen"
];