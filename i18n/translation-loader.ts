import * as config from './i18n';

const translationLoader = {
    type: 'backend',
    init: () => {},
    read: (language: string, namespace: string, callback: Function) => {
        let resource, error = null;

        try {
            // @ts-ignore
            resource = config.supportedLocales[language]
                .translationFileLoader()[namespace];
        } catch (_error) { error = _error; }

        callback(error, resource);
    },
};

export default translationLoader;
