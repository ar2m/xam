const en = 
{
    "common": {
        
    },
    "SplashScreen": {
        "get_started_button": "Get Started",
        "slogan_message": "The simplest way to control your exams sessions !",
        "slogan_sub_message": "Sign in with account"
    }
}

export default en;
