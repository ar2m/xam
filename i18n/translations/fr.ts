const fr = 
{
    "common": {
    },
    "SplashScreen": {
        "get_started_button": "Allez-y",
        "slogan_message": "Restez connectés avec l'école de vos enfants!",
        "slogan_sub_message": "Se connecter avec un compte"
    }
}

export default fr;
