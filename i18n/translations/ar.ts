const ar = 
{
    "common": {
    },
    "SplashScreen": {
        "get_started_button": "إبدأ",
        "slogan_message": "ابق على اتصال مع مدرسة أطفالك!",
        "slogan_sub_message": "تسجيل الدخول باستخدام الحساب"
    }
}

export default ar;
