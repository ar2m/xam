import React from "react";
import { Button, Text, View, StatusBar } from "react-native";
import { useTheme } from "@react-navigation/native";

const HomeScreen = ({ navigation }) => {
  const { colors } = useTheme();
  const theme = useTheme();

  return (
    <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
      <StatusBar barStyle={theme.dark ? "light-content" : "dark-content"} />
      <Text style={{ color: colors.text }}>Home Screen</Text>
      <Button
        title="Go to Details"
        onPress={() => navigation.navigate("Details")}
      />
    </View>
  );
};

export default HomeScreen;
