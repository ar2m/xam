import React, { useEffect, useState } from "react";
import { Container, Content, List, Toast } from "native-base";
import { ActivityIndicator, View, Text, StyleSheet } from "react-native";

import NewsCard from "../components/NewsCard";

import { fetchNews } from "../services/news";

const NewsScreen = ({ navigation }) => {
  const [news, setNews] = useState([]);
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    fetchNews()
      .then((news) => setNews(news))
      .then(() => setIsLoading(false))
      .catch((error) =>
        Toast.show({
          text: "Something went wrong!",
          buttonText: "OK",
          position: "bottom",
          type: "danger",
          buttonTextStyle: { color: "#008000" },
          buttonStyle: { backgroundColor: "#5cb85c" },
          duration: 3000,
        })
      );
  }, []);

  if (isLoading) {
    return (
      <View style={styles.container}>
        <ActivityIndicator size="large" animating={isLoading} />
        <Text style={{ marginTop: 10 }}>Please waiting...</Text>
      </View>
    );
  }

  return (
    <Container>
      <Content>
        <List
          dataArray={news}
          renderRow={(item, index) => <NewsCard data={item} key={index} />}
        />
      </Content>
    </Container>
  );
};

export default NewsScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
});
