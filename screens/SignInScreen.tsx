import React, { useContext } from "react";
import {
  Dimensions,
  TouchableOpacity,
  View,
  Text,
  Platform,
  StyleSheet,
  TextInput,
  StatusBar,
  Alert,
} from "react-native";
import { isEmpty, trim } from "lodash";
import { LinearGradient } from "expo-linear-gradient";
import Feather from "react-native-vector-icons/Feather";
import FontAwesome from "react-native-vector-icons/FontAwesome";
import * as Animatable from "react-native-animatable";
import { useTheme } from "@react-navigation/native";

import AuthContext from "../state/context/authContext";
import { MAIN_COLOR, MAIN_COLOR_GRADIENT } from "../utils/constants";

const SignInScreen = ({ navigation }) => {
  const { colors } = useTheme();

  const [data, setData] = React.useState({
    email: "",
    password: "",
    check_textInputChange: false,
    secureTextEntry: true,
    isValidUser: true,
    isValidPassword: true,
  });

  const { signIn } = useContext(AuthContext);

  const handleTextInputChange = (val: string) => {
    if (!isEmpty(val) && val.length >= 4) {
      setData({
        ...data,
        email: val,
        check_textInputChange: true,
        isValidUser: true,
      });
    } else {
      setData({
        ...data,
        email: val,
        check_textInputChange: false,
        isValidUser: false,
      });
    }
  };

  const handlePasswordChange = (val: string) => {
    if (!isEmpty(val) && val.length >= 8) {
      setData({
        ...data,
        password: val,
        isValidPassword: true,
      });
    } else {
      setData({
        ...data,
        password: val,
        isValidPassword: false,
      });
    }
  };

  const updateSecureTextEntry = () => {
    setData({
      ...data,
      secureTextEntry: !data.secureTextEntry,
    });
  };

  const handleValidUser = (e) => {
    const value = e.nativeEvent.text;
    console.log(trim(value).length);
    setData({ ...data, isValidUser: trim(value).length >= 4 ? true : false });
  };

  const loginHandler = (userName: string, password: string) => {
    const mock = Math.round(Math.random() * 10);

    if (isEmpty(userName) || isEmpty(password)) {
      Alert.alert("Wrong input", "Username or password can not be empty", [
        { text: "OK" },
      ]);
      return;
    }

    if (mock <= 3) signIn(userName, password);
    else {
      Alert.alert("Invalid user", "Username or password is incorrect", [
        { text: "OK" },
      ]);
      return;
    }
  };

  return (
    <View style={styles.container}>
      <StatusBar backgroundColor={MAIN_COLOR} barStyle="light-content" />
      <View style={styles.header}>
        <Text style={styles.text_header}>Welcome !</Text>
      </View>
      <Animatable.View
        animation="fadeInUpBig"
        style={[styles.footer, { backgroundColor: colors.background }]}
      >
        <Text style={[styles.text_footer, { color: colors.text }]}>Email</Text>
        <View style={styles.action}>
          <FontAwesome name="user-o" color={colors.text} size={20} />
          <TextInput
            style={[styles.textInput, { color: colors.text }]}
            placeholder="Your email"
            placeholderTextColor="#666"
            autoCapitalize="none"
            onChangeText={(val) => handleTextInputChange(val)}
            onEndEditing={(e) => handleValidUser(e)}
          />
          {data.check_textInputChange ? (
            <Animatable.View animation="bounceIn">
              <Feather name="check-circle" color="green" size={18} />
            </Animatable.View>
          ) : null}
        </View>
        {data.isValidUser ? null : (
          <Animatable.View animation="fadeInLeft" duration={500}>
            <Text style={styles.errorMsg}>
              Username must be 4 characters long
            </Text>
          </Animatable.View>
        )}

        <Text
          style={[styles.text_footer, { marginTop: 35, color: colors.text }]}
        >
          Password
        </Text>
        <View style={styles.action}>
          <Feather name="lock" color={colors.text} size={20} />
          <TextInput
            style={[styles.textInput, { color: colors.text }]}
            placeholder="Your password"
            placeholderTextColor="#666"
            autoCapitalize="none"
            secureTextEntry={data.secureTextEntry}
            onChangeText={(val) => handlePasswordChange(val)}
          />
          <TouchableOpacity onPress={updateSecureTextEntry}>
            <Feather
              name={data.secureTextEntry ? "eye-off" : "eye"}
              color="grey"
              size={18}
            />
          </TouchableOpacity>
        </View>
        {data.isValidPassword ? null : (
          <Animatable.View animation="fadeInLeft" duration={500}>
            <Text style={styles.errorMsg}>
              Password must be 8 characters long
            </Text>
          </Animatable.View>
        )}

        <View style={styles.button}>
          <TouchableOpacity
            style={[styles.signIn]}
            onPress={() => loginHandler(data.email, data.password)}
          >
            <LinearGradient
              colors={[MAIN_COLOR, MAIN_COLOR_GRADIENT]}
              style={styles.signIn}
            >
              <Text style={[styles.textSign, { color: "white" }]}>Sign In</Text>
            </LinearGradient>
          </TouchableOpacity>

          <TouchableOpacity
            style={[
              styles.signIn,
              { borderColor: MAIN_COLOR, borderWidth: 1, marginTop: 15 },
            ]}
            onPress={() => navigation.navigate("SignUpScreen")}
          >
            <Text style={[styles.textSign, { color: MAIN_COLOR }]}>
              Sign Up
            </Text>
          </TouchableOpacity>
        </View>
      </Animatable.View>
    </View>
  );
};

export default SignInScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: MAIN_COLOR,
  },
  header: {
    flex: 1,
    justifyContent: "flex-end",
    paddingHorizontal: 20,
    paddingBottom: 50,
  },
  footer: {
    flex: 3,
    backgroundColor: "#fff",
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    paddingHorizontal: 20,
    paddingVertical: 30,
  },
  text_header: {
    color: "#fff",
    fontWeight: "bold",
    fontSize: 30,
  },
  text_footer: {
    color: "#05375a",
    fontSize: 18,
  },
  action: {
    flexDirection: "row",
    marginTop: 10,
    borderBottomWidth: 1,
    borderBottomColor: "#f2f2f2",
    paddingBottom: 5,
  },
  actionError: {
    flexDirection: "row",
    marginTop: 10,
    borderBottomWidth: 1,
    borderBottomColor: "#FF0000",
    paddingBottom: 5,
  },
  textInput: {
    flex: 1,
    marginTop: Platform.OS === "ios" ? 0 : -12,
    paddingLeft: 10,
    color: "#05375a",
  },
  errorMsg: {
    color: "#FF0000",
    fontSize: 14,
  },
  button: {
    alignItems: "center",
    marginTop: 50,
  },
  signIn: {
    width: "100%",
    height: 50,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 10,
  },
  textSign: {
    fontSize: 18,
    fontWeight: "bold",
  },
});
