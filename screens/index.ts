import HomeScreen from './HomeScreen'
import DetailsScreen from './DetailsScreen'
import NewsScreen from './NewsScreen'
import ProfileScreen from './ProfileScreen'
import ExploreScreen from './ExploreScreen'
import SignInScreen from './SignInScreen'
import SignUpScreen from './SignUpScreen'
import SplashScreen from './SplashScreen'

export { HomeScreen, DetailsScreen, NewsScreen, ProfileScreen, ExploreScreen, SignInScreen, SignUpScreen, SplashScreen }