import React from "react";
import { Button, Text, View } from "react-native";

const DetailsScreen = ({ navigation }) => (
  <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
    <Text>Details Screen</Text>
    <Button title="Go to Home" onPress={() => navigation.navigate("Home")} />
    <Button
      title="Go to Details again..."
      onPress={() => navigation.push("Details")}
    />
    <Button title="Go Back" onPress={() => navigation.goBack()} />
    <Button title="Go to First Screen" onPress={() => navigation.popToTop()} />
  </View>
);

export default DetailsScreen;
