import React from "react";
import { createMaterialBottomTabNavigator } from "@react-navigation/material-bottom-tabs";
import { AntDesign as Icon } from "@expo/vector-icons";

import {
  HomeScreenStack,
  DetailsScreenStack,
  NewsScreenStack,
} from "../stacks";
import { ExploreScreen, ProfileScreen } from "../screens";
import {
  TEXT_SELECTED,
  MAIN_COLOR,
  TEXT_INACTIVE,
  WHITE,
} from "../utils/constants";

const Tab = createMaterialBottomTabNavigator();

const Tabs = () => (
  <Tab.Navigator
    initialRouteName="News"
    activeColor={MAIN_COLOR}
    inactiveColor={TEXT_INACTIVE}
    barStyle={{ backgroundColor: WHITE }}
    tabBarOptions="true"
  >
    <Tab.Screen
      name="News"
      component={NewsScreenStack}
      options={{
        tabBarLabel: "News",
        tabBarIcon: ({ color }) => <Icon name="bars" color={color} size={25} />,
      }}
    />

    <Tab.Screen
      name="Home"
      component={HomeScreenStack}
      options={{
        tabBarLabel: "Home",
        tabBarIcon: ({ color }) => <Icon name="home" color={color} size={25} />,
      }}
    />
    <Tab.Screen
      name="Details"
      component={DetailsScreenStack}
      options={{
        tabBarLabel: "Details",
        tabBarIcon: ({ color }) => (
          <Icon name="appstore-o" color={color} size={25} />
        ),
      }}
    />
    <Tab.Screen
      name="Explore"
      component={ExploreScreen}
      options={{
        tabBarLabel: "Explore",
        tabBarIcon: ({ color }) => (
          <Icon name="search1" color={color} size={25} />
        ),
      }}
    />
    <Tab.Screen
      name="Profile"
      component={ProfileScreen}
      options={{
        tabBarLabel: "Profile",
        tabBarIcon: ({ color }) => <Icon name="user" color={color} size={25} />,
      }}
    />
  </Tab.Navigator>
);

export default Tabs;
