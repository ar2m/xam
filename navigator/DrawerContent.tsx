import React, { useContext } from "react";
import { View, StyleSheet, Dimensions } from "react-native";
import { DrawerContentScrollView } from "@react-navigation/drawer";
import {
  useTheme,
  Avatar,
  Title,
  Caption,
  Paragraph,
  Drawer,
  Text,
  TouchableRipple,
  Switch,
} from "react-native-paper";

import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import * as Animatable from "react-native-animatable";

import AuthContext from "../state/context/authContext";

const DrawerContent = (props) => {
  //const toggleTheme = () => setIsDarkTheme(!isDarkTheme);
  const { signOut, toggleTheme } = useContext(AuthContext);

  const paperTheme = useTheme();

  return (
    <View style={{ flex: 1 }}>
      <DrawerContentScrollView {...props}>
        <View style={styles.drawerContent}>
          <View style={styles.userInfoSection}>
            <View style={{ flexDirection: "row", marginTop: 15 }}>
              <Avatar.Image
                source={require("../assets/me-at-flamingo.jpg")}
                size={50}
              />
              <View style={{ flexDirection: "column", marginLeft: 8 }}>
                <Title style={styles.title}>Mohamed Hamidi</Title>
                <Caption style={styles.caption}>mhamidi@gmail.com</Caption>
              </View>
            </View>
            <View style={styles.row}>
              <View style={styles.section}>
                <Paragraph style={[styles.paragraph, styles.caption]}>
                  100
                </Paragraph>
                <Caption style={styles.caption}>Following</Caption>
              </View>
              <View style={styles.section}>
                <Paragraph style={[styles.paragraph, styles.caption]}>
                  200
                </Paragraph>
                <Caption style={styles.caption}>Followers</Caption>
              </View>
            </View>
          </View>
        </View>
        <Drawer.Section style={styles.drawerSection}>
          <Drawer.Item
            label="Home"
            icon={({ color, size }) => (
              <Icon name="home-outline" color={color} size={size} />
            )}
            onPress={() => {
              props.navigation.navigate("Home");
            }}
          />
          <Drawer.Item
            label="Details"
            icon={({ color, size }) => (
              <Icon name="bell-outline" color={color} size={size} />
            )}
            onPress={() => {
              props.navigation.navigate("Details");
            }}
          />
          <Drawer.Item
            label="Explore"
            icon={({ color, size }) => (
              <Icon name="bookmark-outline" color={color} size={size} />
            )}
            onPress={() => {
              props.navigation.navigate("Explore");
            }}
          />
          <Drawer.Item
            label="Profile"
            icon={({ color, size }) => (
              <Icon name="account-outline" color={color} size={size} />
            )}
            onPress={() => {
              props.navigation.navigate("Profile");
            }}
          />
        </Drawer.Section>
        <Drawer.Section title="Preferences">
          <TouchableRipple onPress={() => toggleTheme()}>
            <View style={styles.preference}>
              <Text>Dark Theme</Text>
              <View pointerEvents="none">
                <Switch value={paperTheme.dark} />
              </View>
            </View>
          </TouchableRipple>
        </Drawer.Section>
      </DrawerContentScrollView>
      <View style={styles.header}>
        <Animatable.Image
          animation="bounceIn"
          source={require("../assets/ibn-zohr-logo.png")}
          style={styles.logo}
          resizeMode="stretch"
        />
      </View>
      <Drawer.Section style={styles.bottomDrawerSection}>
        <Drawer.Item
          label="Sign Out"
          icon={({ color, size }) => (
            <Icon name="exit-to-app" color={color} size={size} />
          )}
          onPress={() => signOut()}
        />
      </Drawer.Section>
    </View>
  );
};

const { height, width } = Dimensions.get("screen");
const height_logo = height * 0.3;
const width_logo = width * 0.15;

const styles = StyleSheet.create({
  drawerContent: {
    flex: 1,
  },
  userInfoSection: {
    paddingLeft: 20,
  },
  title: {
    fontSize: 16,
    marginTop: 3,
    fontWeight: "bold",
  },
  caption: {
    fontSize: 14,
    lineHeight: 14,
  },
  row: {
    marginTop: 20,
    flexDirection: "row",
    alignItems: "center",
  },
  section: {
    flexDirection: "row",
    alignItems: "center",
    marginRight: 15,
  },
  paragraph: {
    fontWeight: "bold",
    marginRight: 3,
  },
  drawerSection: {
    marginTop: 15,
  },
  bottomDrawerSection: {
    marginBottom: 15,
    borderTopColor: "#f4f4f4",
    borderTopWidth: 1,
  },
  preference: {
    flexDirection: "row",
    justifyContent: "space-between",
    paddingVertical: 12,
    paddingHorizontal: 16,
  },
  header: {
    flex: 1,
    flexDirection: "column-reverse",
    alignItems: "center",
  },
  logo: {
    width: height_logo,
    height: width_logo,
  },
});

export default DrawerContent;
