import HomeScreenStack from './HomeScreenStack'
import DetailsScreenStack from './DetailsScreenStack'
import NewsScreenStack from './NewsScreenStack'

export { HomeScreenStack, DetailsScreenStack, NewsScreenStack }