import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import { Ionicons as Icon } from "@expo/vector-icons";

import { NewsScreen } from "../screens";
import { TEXT_DARK, WHITE, NAVIGATOR_STACK_STYLE } from "../utils/constants";

const DetailsStack = createStackNavigator();

const DetailsScreenStack = ({ navigation }) => (
  <DetailsStack.Navigator screenOptions={NAVIGATOR_STACK_STYLE}>
    <DetailsStack.Screen
      name="News"
      component={NewsScreen}
      options={{
        headerLeft: () => (
          <Icon.Button
            name="ios-menu"
            size={26}
            color={TEXT_DARK}
            backgroundColor={WHITE}
            onPress={() => navigation.openDrawer()}
          />
        ),
      }}
    />
  </DetailsStack.Navigator>
);

export default DetailsScreenStack;
