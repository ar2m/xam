import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import { Ionicons as Icon } from "@expo/vector-icons";

import { HomeScreen } from "../screens";
import { TEXT_DARK, WHITE, NAVIGATOR_STACK_STYLE } from "../utils/constants";

const HomeStack = createStackNavigator();

const HomeScreenStack = ({ navigation }) => (
  <HomeStack.Navigator screenOptions={NAVIGATOR_STACK_STYLE}>
    <HomeStack.Screen
      name="Home"
      component={HomeScreen}
      options={{
        headerLeft: () => (
          <Icon.Button
            name="ios-menu"
            size={26}
            color={TEXT_DARK}
            backgroundColor={WHITE}
            onPress={() => navigation.openDrawer()}
          />
        ),
      }}
    />
  </HomeStack.Navigator>
);

export default HomeScreenStack;
