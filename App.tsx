import * as Updates from "expo-updates";
import React, { useState, useEffect, useMemo, useReducer } from "react";
import {
  ActivityIndicator,
  StyleSheet,
  View,
  I18nManager as RNI18nManager,
} from "react-native";
import { NavigationContainer } from "@react-navigation/native";
import { Provider as PaperProvider } from "react-native-paper";
import { createDrawerNavigator } from "@react-navigation/drawer";
import AsyncStorage from "@react-native-community/async-storage";

import Tabs from "./navigator/Tabs";
import DrawerContent from "./navigator/DrawerContent";
import RootStackScreen from "./navigator/RootStackScreen";
import AuthContext from "./state/context/authContext";
import { loginReducer } from "./state/reducers/loginReducer";
import {
  RETRIEVE_TOKEN_ACTION,
  SIGNIN_ACTION,
  SIGNOUT_ACTION,
  SIGNUP_ACTION,
} from "./state/actions";

import { CustomDefaultTheme, CustomDarkTheme } from "./themes";
import i18n from "./i18n";

const Drawer = createDrawerNavigator();

export default function App() {
  const [isDarkTheme, setIsDarkTheme] = React.useState(false);
  const [isI18nInitialized, setIsI18nInitialized] = React.useState(false);

  const initialLoginState = {
    isLoading: true,
    userName: null,
    userToken: null,
  };

  const theme = isDarkTheme ? CustomDarkTheme : CustomDefaultTheme;

  const [loginState, dispatch] = useReducer(loginReducer, initialLoginState);

  const authContext = useMemo(() => ({
    signIn: async (userName, password) => {
      let userToken = null;
      //if (userName === "user" && password === "pass") {
      try {
        userToken = "azerty";
        await AsyncStorage.setItem("userToken", userToken);
      } catch (error) {
        console.log(error);
      }
      //}
      dispatch({ type: SIGNIN_ACTION, userName, userToken });
    },
    signOut: async () => {
      try {
        await AsyncStorage.removeItem("userToken");
      } catch (error) {
        console.log(error);
      }
      dispatch({ type: SIGNOUT_ACTION });
    },
    signUp: () => {
      setIsLoading(false);
      setUserToken(loginState);
    },
    toggleTheme: () => {
      setIsDarkTheme(!isDarkTheme);
    },
  }));

  useEffect(() => {
    i18n
      .init()
      .then(() => {
        const RNDir = RNI18nManager.isRTL ? "RTL" : "LTR";
        // RN doesn't always correctly identify native
        // locale direction, so we force it here.
        if (i18n.dir() !== RNDir) {
          const isLocaleRTL = i18n.dir() === "RTL";
          RNI18nManager.forceRTL(isLocaleRTL);
          // RN won't set the layout direction if we
          // don't restart the app's JavaScript.
          Updates.reloadAsync();
        }
        setIsI18nInitialized(true);
      })
      .catch((error) => console.warn(error));
    return () => {};
  }, []);

  useEffect(() => {
    setTimeout(async () => {
      let userToken = null;
      try {
        userToken = await AsyncStorage.getItem("userToken");
      } catch (error) {
        console.log(error);
      }
      dispatch({ type: SIGNUP_ACTION, userToken });
    }, 1000);
  });

  if (loginState.isLoading) {
    return (
      <View style={styles.container}>
        <ActivityIndicator size="large" />
      </View>
    );
  }
  return (
    <PaperProvider theme={theme}>
      <AuthContext.Provider value={authContext}>
        <NavigationContainer theme={theme}>
          {loginState.userToken ? (
            <Drawer.Navigator
              drawerContent={(props) => <DrawerContent {...props} />}
            >
              <Drawer.Screen name="HomeDrawer" component={Tabs} />
            </Drawer.Navigator>
          ) : (
            <RootStackScreen />
          )}
          {/* <Drawer.Navigator
            drawerContent={(props) => <DrawerContent {...props} />}
          >
            <Drawer.Screen name="HomeDrawer" component={Tabs} />
          </Drawer.Navigator> */}
        </NavigationContainer>
      </AuthContext.Provider>
    </PaperProvider>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
});
