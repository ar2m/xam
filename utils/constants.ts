export const MAIN_COLOR = "#03989E"
export const MAIN_COLOR_GRADIENT = "#22B7BD"
export const WHITE = "#FFF"
export const TEXT_DARK = "#3B3B3B"
export const TEXT_SELECTED = "#B46088"
export const TEXT_INACTIVE = "#95A5A6"
export const FONT_WEIGHT_BOLD = "bold"
export const NAVIGATOR_STACK_STYLE = {
    headerStyle: {
      backgroundColor: WHITE,
      shadowOffset: { height: 0, width: 0 }
      
    },
    headerTintColor: TEXT_DARK,
    headerTitleStyle: {
      fontWeight: FONT_WEIGHT_BOLD,
      color: MAIN_COLOR
    },
};