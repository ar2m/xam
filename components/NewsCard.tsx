import React from "react";
import PropTypes from "prop-types";
import {
  ListItem,
  Text,
  Left,
  Body,
  Right,
  Card,
  CardItem,
  Thumbnail,
  Button,
} from "native-base";
import { Image, StyleSheet } from "react-native";
import * as moment from "moment";

import { MAIN_COLOR, TEXT_SELECTED } from "../utils/constants";

const NewsCard = ({ data }) => (
  <ListItem noBorder>
    <Body>
      <Card>
        <CardItem>
          <Left>
            <Thumbnail
              small
              source={require("../assets/ibn-zohr-logo-circle.png")}
            />
            <Body>
              <Text numberOfLines="1" style={styles.title}>
                {data.title}
              </Text>
              <Text numberOfLines="1" style={styles.note}>
                {data.source.name}
              </Text>
            </Body>
          </Left>
        </CardItem>
        <CardItem cardBody>
          <Image
            source={{ uri: data.urlToImage }}
            style={{ height: 180, width: null, flex: 1 }}
          />
        </CardItem>
        <CardItem cardBody>
          <Text numberOfLines="2" style={styles.description}>
            {data.description}
          </Text>
        </CardItem>
        <CardItem>
          <Body>
            <Text style={styles.date}>
              {moment.utc(data.publishedAt).format("YYYY-MM-DD")}
            </Text>
            <Text style={styles.time}>
              {moment.utc(data.publishedAt).format("HH:mm:ss")}
            </Text>
          </Body>
          <Right>
            <Button transparent small>
              <Text>Details</Text>
            </Button>
          </Right>
        </CardItem>
      </Card>
    </Body>
  </ListItem>
);

NewsCard.propTypes = {
  data: PropTypes.object.isRequired,
};

export default NewsCard;

const styles = StyleSheet.create({
  title: {
    color: MAIN_COLOR,
    fontSize: 13,
  },
  note: {
    color: TEXT_SELECTED,
    fontSize: 12,
  },
  description: {
    color: "#708090",
    fontSize: 13,
    paddingLeft: 3,
    paddingRight: 2,
  },
  date: {
    color: "#A9A9A9",
    fontSize: 12,
  },
  time: {
    color: "#A9A9A9",
    fontSize: 11,
  },
});
